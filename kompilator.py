#!/usr/bin/python3

from typing import List
from sys import argv, exc_info
from base64 import b16encode
from re import sub, findall

import logging as log

log.basicConfig(level=log.DEBUG)

POLECENIA_RR = {
    "AR": ord("a"),
    "SR": ord("s"),
    "MR": ord("m"),
    "DR": ord("d"),
    "CR": ord("c"),
    "LR": ord("l"),
}

POLECENIA_RP = {
    "A": ord("A"),
    "S": ord("S"),
    "M": ord("M"),
    "D": ord("D"),
    "C": ord("C"),
    "L": ord("L"),
    "ST": ord("X"), # od niczego 
    "LA": ord("W"), # od Wskaznik    
}

POLECENIA_SKOKU = {
    "J": ord("J"),
    "JP": ord("P"),
    "JN": ord("N"),
    "JZ": ord("Z"),
}

POLECENIA_REZERWACJI = {
    "DC": ord("#"),
    "DS": ord("%"),
}

POLECENIA_SPECJALNE_0ARG = {
    "STOP": ord("0"),
    "NOOP": ord("1"),
    "PRINTRS": ord("2"),
}

POLECENIA_SPECJALNE_1ARG = {
    "INPUTR": ord("6"),
    "INPUTM": ord("7"),
    "PRINTR": ord("8"),
    "PRINTM": ord("9"),
}

def kompiluj_rr(kod: int, r1: int, r2: int) -> bytes:
    return ((kod<<8) | ((r1<<4)|r2)).to_bytes(2, "big", signed=False)

def kompiluj_rp(kod: int, r:int, radr: int, ofs: int) -> bytes:
    return ((((((kod<<4)|r)<<4)|radr)<<16)|ofs).to_bytes(4, "big", signed=False)

def kompiluj_0arg(kod: int) -> bytes:
    return kod.to_bytes(1, "big", signed=False) + b"\x00"

def kompiluj_printr(kod: int, r: int) -> bytes:
    return ((kod<<8) | ((r<<4)|r)).to_bytes(2, "big", signed=False)

def kompiluj_printreadm(kod: int, radr: int, ofs: int) -> bytes:
    return ((((((kod<<4)|radr)<<4)|radr)<<16)|ofs).to_bytes(4, "big", signed=False)

def dekonstruuj_linie(linia: str, numer: int) -> List[str]:

    log.debug(" Interp. linii %d: \"%s\"" % (numer,linia.strip()))

    czesc_wlasciwa, *_ = linia.split(";") # Wsparcie dla komentarzy
    czesc_wlasciwa = sub(r'\s*,\s*', ',', czesc_wlasciwa) # Usuniecie spacji dookola przecinka
    czesc_wlasciwa = sub(r'\s*\*\s*', '*', czesc_wlasciwa) # Usuniecie spacji dookola gwiazdek
    czesc_wlasciwa = sub(r"\s*\)", ")", czesc_wlasciwa) # TODO: Tymczasowe usuniecie spacji przed nawiasem zamykajacym
    czesc_wlasciwa = sub(r'\s+', ' ', czesc_wlasciwa).strip() # Usuniecie powielonych spacji, tabulatorow i nowych linii
    
    elementy = czesc_wlasciwa.split(" ")

    log.debug("\tWlasciwa \"%s\"" % czesc_wlasciwa)
    log.debug("\tElementy %s" % str(elementy))

    etykieta: str = "_L_%d" % numer
    rozkaz: str = ""
    argumenty = []

    # MAAAAAGIAAAA NIE PATRZEC PONIZEJ

    if len(elementy) == 1:
        rozkaz = elementy[0]

    elif len(elementy) == 2:

        if elementy[0] in POLECENIA_SPECJALNE_1ARG or elementy[0] in POLECENIA_SKOKU:
            rozkaz = elementy[0]
            argumenty = [elementy[1]]

        elif elementy[1] in POLECENIA_SPECJALNE_0ARG:
            etykieta, rozkaz = elementy

        elif "," in elementy[1]:
            rozkaz = elementy[0]
            argumenty = elementy[1].split(",")

    elif len(elementy) == 3:

        if "," in elementy[2]:

            etykieta = elementy[0]
            rozkaz = elementy[1]

            if elementy[1] in POLECENIA_REZERWACJI:
                argumenty = [elementy[2]]
            else:
                argumenty = elementy[2].split(",")

        else: # Niech sie po prostu dzieje to co kiedys

            etykieta = elementy[0]
            rozkaz = elementy[-2]
            argumenty = [elementy[-1]] if rozkaz in POLECENIA_REZERWACJI else elementy[-1].split(",")

    log.debug("\t--WYNIK: %s" % str((etykieta, rozkaz, argumenty)))
    return (etykieta, rozkaz, argumenty)

class Program:

    etykiety: dict = {}
    rezerwacje: dict = {}
    rezerwacje_tablic: dict = {}

    kod: List[str] = []

    sekcja_danych: bytes
    sekcja_rozkazow: bytes

    def __init__(self, zrodlo: str):
        f = open(zrodlo, "r")
        i = 0
        for l in f.readlines():
            if l != "\n":
                self.kod.append(dekonstruuj_linie(l, numer=i))
                i += 1
        f.close()

    def __wczytaj_rezerwacje(self):
        kod_bez_rezerwacji = []
        ofs = 0

        for l in self.kod:
            et, pol, arg = l

            if pol in POLECENIA_REZERWACJI:

                print(et, pol, arg)
                arg = arg[0]
                powtorzen: int = int(arg[:arg.find("*")]) if "*" in arg else 1
                wartosci = powtorzen*[int(w) for w in findall(r"\((.*?)\)", arg)[0].split(",")] if pol=="DC" else [0]
                rozmiar: int = 4*len(wartosci)
                
                log.debug(" Rezerwuje %s rozmiar %d ofs %d, wartosci %s" % (et, rozmiar, ofs, str(wartosci)))
                self.rezerwacje[et] = (rozmiar, ofs, wartosci)
                ofs += rozmiar

            # Wsparcie dla constantow poprzedzonych znakiem $
            elif pol in POLECENIA_RP:
                _, a2 = arg
                if "$" in a2:
                    wartosci = [int(a2[a2.find("$")+1:])]
                    log.debug(" Rezerwuje %s rozmiar 4 ofs %d, wartosci %s" % (a2, ofs, str(wartosci)))
                    self.rezerwacje[a2] = (4, ofs, wartosci)
                    ofs += 4
                kod_bez_rezerwacji.append(l)

            else:
                kod_bez_rezerwacji.append(l)
        
        self.kod = kod_bez_rezerwacji
    
    def __wczytaj_etykiety(self):
        i = 0
        for l in self.kod:
            et, pol, arg = l

            if not et in self.rezerwacje:
                self.etykiety[et] = i
            
            i += 1

    def __kompiluj_sekcje_danych(self):
        # Sekcja zaczyna sie 32-bitowa informacja o dlugosci rezerwacji
        # a nastepnie czytelnym naglowkiem
        lend: int = sum([d[0] for et,d in self.rezerwacje.items()])
        skcd: bytes = lend.to_bytes(4, "big", signed=False) + b"[danei]:"

        for et, dane in self.rezerwacje.items():
            for x in dane[2]:
                skcd += x.to_bytes(4, "big", signed=True)

        self.sekcja_danych = skcd

    def __kompiluj_sekcje_rozkazow(self):
        dlugosci_rozk = [2 if (p[1] in POLECENIA_RR or p[1] in POLECENIA_SPECJALNE_0ARG) else 4 for p in self.kod] 
        # Sekcja zaczyna sie 32-bitowa informacja o calkowitej dlugosci rozkazow
        # a nastepnie czytelnym naglowkiem
        skcr: bytes = sum(dlugosci_rozk).to_bytes(4, "big", signed=False) + b"[rozkz]:"

        for et, pol, arg in self.kod:
            
            try:
                if pol in POLECENIA_RR:
                    skcr += kompiluj_rr(POLECENIA_RR[pol], int(arg[0]), int(arg[1]))

                elif pol in POLECENIA_RP:
                    r, a = arg; r = int(r) # Argumenty
                    rej_adr = 14; ofs: int = 0

                    log.debug("r %d arg %s" % (r,str(a)))

                    if a in self.rezerwacje:
                        ofs = self.rezerwacje[a][1] # Automatycznie przydzielony ofs
                    elif "(" in a:
                        # Manualnie zdefiniowany ofs(rej_adr)
                        rej_adr = int( a[a.find("(")+1 : a.find(")")] )
                        ofs = int( a[0:a.find("(")] )
                    # else: cos zlego sie stanie hehe

                    skcr += kompiluj_rp(POLECENIA_RP[pol], r, rej_adr, ofs)
                
                elif pol in POLECENIA_SKOKU:
                    a = arg[0]
                    if "(" in a:
                        rej_adr = int( a[a.find("(")+1 : a.find(")")] )
                        ofs = int( a[0:a.find("(")] )
                        skcr += kompiluj_rp(POLECENIA_SKOKU[pol], 0, rej_adr, ofs)
                    else:
                        gdzie_to_bedzie = sum(dlugosci_rozk[:self.etykiety[arg[0]]])
                        skcr += kompiluj_rp(POLECENIA_SKOKU[pol], 0, 15, gdzie_to_bedzie)

                elif pol in POLECENIA_SPECJALNE_0ARG:
                    skcr += kompiluj_0arg(POLECENIA_SPECJALNE_0ARG[pol])
                
                elif pol in POLECENIA_SPECJALNE_1ARG:

                    if pol == "PRINTR":
                        skcr += kompiluj_printr(POLECENIA_SPECJALNE_1ARG[pol], int(arg[0]))

                    elif pol in ["PRINTM", "INPUTM"]:
                        
                        a = arg[0]
                        rej_adr: int = 14
                        ofs: int = 0
                        
                        if a in self.rezerwacje:
                            ofs = self.rezerwacje[a][1]

                        elif "(" in a:
                            # Manualnie zdefiniowany ofs(rej_adr)
                            rej_adr = int( a[a.find("(")+1 : a.find(")")] )
                            ofs = int( a[0:a.find("(")] )
                        
                        skcr += kompiluj_printreadm(POLECENIA_SPECJALNE_1ARG[pol], rej_adr, ofs)
                        
                else:
                    raise RuntimeError("Nieznane polecenie %s." % pol)
            except:
                print("")
        self.sekcja_rozkazow = skcr

    def wypisz_kod(self):
        i = 0
        for l in self.kod:
            print(i, l)
            i += 1
    
    def kompiluj(self, debug: bool = True):
        self.__wczytaj_rezerwacje()
        self.__kompiluj_sekcje_danych()
        self.__wczytaj_etykiety()
        self.__kompiluj_sekcje_rozkazow()

        if debug:
            print(self.rezerwacje)
            print(self.etykiety)
            print(self.sekcja_danych)
            print(self.sekcja_rozkazow)

    def zapisz(self, wyjscie: str):
        f = open(wyjscie, "wb")
        f.seek(0); f.truncate()
        f.write(b"PSAS01PW" + self.sekcja_danych + self.sekcja_rozkazow + b"WP10SASP")
        f.close()

if __name__ == "__main__":
    
    argc = len(argv)
    nazwa_zrodla: str
    nazwa_wyjscia: str

    if argc < 2:
        print("Nie podano argumentow.\n\nSposob uzycia:")
        print("\tpython3 kompilator.py [nazwa_zrodla] (nazwa_wyjscia)")
        quit()

    if argc >= 3:
        nazwa_zrodla = argv[1]
        nazwa_wyjscia = argv[2]
    else:
        nazwa_zrodla = argv[1]
        nazwa_wyjscia = nazwa_zrodla + ".wyk"

    p = Program(nazwa_zrodla)
    p.kompiluj()
    p.zapisz(nazwa_wyjscia)
