#include <cstdio>
#include <cstdlib>

#include <map>
#include <tuple>
#include <regex>
#include <memory>
#include <string>
#include <vector>

#include <iostream>
#include <fstream>

#define slownikpol_t std::map<string, char>
#define vstr_t std::vector<string>

using namespace std;

struct rezerw_t 
{
    int rozmiar, ofs;
    vector<int> wartosci;
};

struct dekonstr_t
{
    string etykieta, polecenie;
    vector<string> argumenty;
};

const slownikpol_t POLECENIA_RR = {
    {"AR", 'a'},
    {"SR", 's'},
    {"MR", 'm'},
    {"DR", 'd'},
    {"CR", 'c'},
    {"LR", 'l'},
};

const slownikpol_t POLECENIA_RP = {
    {"A", 'A'},
    {"S", 'S'},
    {"M", 'M'},
    {"D", 'D'},
    {"C", 'C'},
    {"L", 'L'},
    {"ST", 'X'},
    {"LA", 'W'},
};

const slownikpol_t POLECENIA_SKOKU = {
    {"J",  'J'},
    {"JP", 'P'},
    {"JN", 'N'},
    {"JZ", 'Z'},
};

const slownikpol_t POLECENIA_SPECJALNE_RP = {
    {"INPUTM", '7'},
    {"PRINTM", '9'},
};

const slownikpol_t POLECENIA_SPECJALNE_0ARG = {
    {"STOP",    '0'},
    {"NOOP",    '1'},
    {"PRINTRS", '2'},
};

const slownikpol_t POLECENIA_SPECJALNE_1ARG = {
    {"PRINTR", '8'},
};

const slownikpol_t DYREKTYWY_REZERWACJI = {
    {"DC", '#'},
    {"DS", '%'},
};

bool w_slowniku(string pol, slownikpol_t slownikpol)
{
    for (auto const& x : slownikpol) if (x.first == pol) return true;
    return false;
}

vstr_t wczytaj_plik_liniami(string nazwa)
{
    vstr_t linie;
    string linia;

    ifstream plik(nazwa);

    while (getline(plik, linia))
        linie.push_back(linia);

    plik.close();
    return linie;
}

vstr_t podziel(string napis, char dzielnik)
{
    stringstream ss(napis); string czesc;
    vstr_t podzial;

    // https://stackoverflow.com/questions/10058606/splitting-a-string-by-a-character/10058756
    // Komentarz Jarka C z 23 maja 2018 r. sugeruje uprzednie sprawdzenie
    // liczby wystapien dzielnika w dzielonym napisie celem zarezerwowania
    // tylu miejsc w wektorze podzialu.
    podzial.reserve(count_if(napis.begin(), napis.end(), [&](char c) {return c == dzielnik;}) + (napis.empty() ? 1 : 0));

    while(getline(ss, czesc, dzielnik))
        podzial.push_back(czesc);

    return podzial;
}

const regex DOOK_PRZECINKA("\\s*,\\s*");
const regex DOOK_GWIAZDKI("\\s*\\*\\s*");
const regex PRZED_NAWIASEM_ZAM("\\s*\\)");
const regex POWIELONE_ODSTEPY("\\s+");

int zawiera(string napis, char znak)
{
    int ile=0;
    for (int i=0; i<napis.length(); ++i)
        if (napis[i] == znak) ++ile;
    return ile;
}

dekonstr_t dekonstruuj_linie(string linia, int numer)
{
    string etykieta="_L_"+to_string(numer), polecenie="UNDEF";
    vstr_t argumenty;

    string format = linia;
    format = regex_replace(format, DOOK_PRZECINKA,      ",");
    format = regex_replace(format, DOOK_GWIAZDKI,       "*");
    format = regex_replace(format, PRZED_NAWIASEM_ZAM,  ")");
    format = regex_replace(format, POWIELONE_ODSTEPY,   " ");

    vstr_t elementy = podziel(format, ' ');
    int len_el = elementy.size();

    if (len_el == 1)
    {
        polecenie = elementy[0];
    }
    else if (len_el == 2)
    {
        if (w_slowniku(elementy[0], POLECENIA_SKOKU)
         || w_slowniku(elementy[0], POLECENIA_SPECJALNE_1ARG)
         || w_slowniku(elementy[0], POLECENIA_SPECJALNE_RP))
        {
            polecenie = elementy[0];
            argumenty.push_back(elementy[1]);
        }
        else if (w_slowniku(elementy[1], POLECENIA_SPECJALNE_0ARG))
        {
            etykieta = elementy[0];
            polecenie = elementy[1];
        }
        else if (zawiera(elementy[1], ','))
        {
            polecenie = elementy[0];
            argumenty = podziel(elementy[1], ',');
        }
    }
    else if (len_el == 3)
    {
        etykieta = elementy[0];
        polecenie = elementy[1];

        if (w_slowniku(polecenie, DYREKTYWY_REZERWACJI))
            argumenty.push_back(elementy[2]);
        else
            argumenty = podziel(elementy[2], ',');
    }

    dekonstr_t dekonstrukcja = {etykieta, polecenie, argumenty};
    return dekonstrukcja;
}

const regex WEWN_NAWIASOW("\\((.*?)\\)");

string na_bajtach(int wartosc, int na_ilu)
{
    string wynik;

    for (int i=0; i<na_ilu; ++i)
    {
        wynik = (char)(wartosc & 0xff) + wynik;
        wartosc >>= 8;
    }

    return wynik;
}

class Program
{
private:
    vector<string> linie_kodu;
    vector<dekonstr_t> dekonstrukcje;
    map<string, rezerw_t> rezerwacje;
    map<string, int> etykiety;
    string sekcja_danych, sekcja_rozkazow;

    void analiza_linii()
    {
        for (int nr_l=0; nr_l<linie_kodu.size(); nr_l++)
            dekonstrukcje.push_back(dekonstruuj_linie(linie_kodu[nr_l], nr_l));
    }

    void wczytanie_rezerwacji()
    {
        vector<dekonstr_t> nowe_dekonstrukcje;
        string t_etykieta, t_polecenie, t_arg;
        int t_razy, ofs = 0, liczba_w;
        
        for (int i=0; i<dekonstrukcje.size(); ++i)
        {
            t_etykieta  = dekonstrukcje[i].etykieta;
            t_polecenie = dekonstrukcje[i].polecenie;

            if (DYREKTYWY_REZERWACJI.count(t_polecenie))
            {
                t_arg = dekonstrukcje[i].argumenty[0];
                t_razy = zawiera(t_arg, '*') ? atoi(&t_arg.substr(0, t_arg.find('*'))[0]) : 1;

                int n1 = t_arg.find('('), n2 = t_arg.find(')');
                
                vector<string> s_wartosci = podziel(t_arg.substr(n1+1, n2-n1-1), ',');
                vector<int> wartosci;

                liczba_w = t_razy * s_wartosci.size();
                wartosci.reserve(liczba_w);

                for (int j=0; j<t_razy; ++j)
                    for (string s : s_wartosci)
                        wartosci.push_back(atoi(&s[0]));

                rezerw_t rezerwacja = {4*liczba_w, ofs, wartosci};
                rezerwacje[t_etykieta] = rezerwacja;
                ofs += 4*liczba_w;
            }
            else 
            {
                if (POLECENIA_RP.count(t_polecenie))
                {
                    t_arg = dekonstrukcje[i].argumenty[1];

                    if (zawiera(t_arg, '$'))
                    {
                        int n1 = t_arg.find('$');
                        string w = t_arg.substr(n1, t_arg.length()-n1-1);

                        vector<int> wartosci = {atoi(&w[0])};

                        rezerw_t rezerwacja = {4, ofs, wartosci};
                        rezerwacje[t_etykieta] = rezerwacja;
                        ofs += 4;
                    }
                }

                nowe_dekonstrukcje.push_back(dekonstrukcje[i]);
            }
        }

        dekonstrukcje = nowe_dekonstrukcje;
    }

    void wczytanie_etykiet()
    {
        string t_etykieta;

        for (int i=0; i<dekonstrukcje.size(); ++i)
        {
            t_etykieta = dekonstrukcje[i].etykieta;

            if (!rezerwacje.count(t_etykieta))
                etykiety[t_etykieta] = i;
        }
    }

    void utworzenie_sekcji_danych()
    {
        int t_rozm, dl_d = 0, ofs_r = 0;
        vector<int> t_wartosci;
        string skcd = "";

        for (auto const& x : rezerwacje)
        {
            t_rozm = x.second.rozmiar;
            dl_d += t_rozm;

            t_wartosci = x.second.wartosci;
            rezerwacje[x.first].ofs = ofs_r;

            for (int i=0; i<t_wartosci.size(); ++i)
                skcd += na_bajtach(t_wartosci[i], 4);

            ofs_r += t_rozm;
        }

        sekcja_danych = na_bajtach(skcd.length(), 4) + skcd;
    }

    void utworzenie_sekcji_rozkazow()
    {
        vector<uint8_t> dlugosci_rozk;
        dlugosci_rozk.reserve(dekonstrukcje.size());

        vector<string> t_argi;
        string t_etykieta, t_polecenie, t_arg;

        for (int i=0; i<dekonstrukcje.size(); ++i)
        {
            t_polecenie = dekonstrukcje[i].polecenie;
            dlugosci_rozk[i] = (
                    POLECENIA_RR.count(t_polecenie) 
                |   POLECENIA_SPECJALNE_0ARG.count(t_polecenie) ) ? 2 : 4;
        }

        for (int i=0; i<dekonstrukcje.size(); ++i)
        {
            t_etykieta  = dekonstrukcje[i].etykieta;
            t_polecenie = dekonstrukcje[i].polecenie;
            t_argi      = dekonstrukcje[i].argumenty;

            
        }
            

    }

public:

    Program(vector<string> linie)
    {
        linie_kodu = linie;
        analiza_linii();
    }

    void kompilacja()
    {
        wczytanie_rezerwacji();
        wczytanie_etykiet();
        utworzenie_sekcji_danych();
        
    }
    
};

int main(int argc, char const *argv[])
{
    string nazwa_zrodla, nazwa_wyjscia;

    if (argc < 2)
    {
        printf("Nie podano argumentow.\n\nSposob uzycia:");
        printf("\t./<kompilator> <nazwa_zrodla> [nazwa_wyjscia]");
        return 1;
    }
    
    nazwa_zrodla = (string) argv[1];
    nazwa_wyjscia = (argc >= 3) ? (string) argv[2] : nazwa_zrodla + ".wyk";

    vector<string> linie_zrodla = wczytaj_plik_liniami(nazwa_zrodla);

    Program program(linie_zrodla);
    program.kompilacja();

    return 0;
}
