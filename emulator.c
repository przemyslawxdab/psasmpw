#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"

// Emulowanie
#define MAX_WYKONANYCH_ROZKAZOW 10000

// Procesor
#define ILOSC_REJESTROW 16
#define REJ_POLOZENIA_ROZKAZOW ILOSC_REJESTROW-1
#define REJ_POLOZENIA_DANYCH ILOSC_REJESTROW-2

// Pamiec (obcieta o 10 bo po co tyle poki co)
#define ROZMIAR_PAMIECI 6553//6
#define POLOZENIE_DANYCH 1000//0
#define POLOZENIE_ROZKAZOW 5000//0

// Plik wykonywalny i jego czesci
#define NAGLOWEK_PLIKU "PSAS01PW"
#define DLUGOSC_NAGLOWKA_PLIKU 8

#define DLUGOSC_INFO_DLDANYCH 4
#define NAGLOWEK_DANYCH "[danei]:"
#define DLUGOSC_NAGLOWKA_DANYCH 8

#define DLUGOSC_INFO_DLROZKAZ 4
#define NAGLOWEK_ROZKAZOW "[rozkz]:"
#define DLUGOSC_NAGLOWKA_ROZKAZOW 8

#define KONCOWKA_PLIKU "WP10SASP"
#define DLUGOSC_KONCOWKI_PLIKU 8

// Skroty typow
#define ip int32_t*
#define uip int32_t*
#define ucp uint8_t*

#define ui uint32_t
#define uc uint8_t

int DEBUG = 0;

void zeruj_pamiec(ucp m)
{
    for (int i=0; i<ROZMIAR_PAMIECI; ++i) m[i] = 0;
}

void podglad_slow(ucp m, ui adr0, ui n, ui po)
{
    for (int b=adr0; b<adr0+4*n; b+=po)
    {
        printf("%.4x: ", b);
        for (int i=0; i<po; i++)
            printf("%.2x ", (uint8_t) m[b+i]);
        printf("\n");
    }
}

void zeruj_rejestry(ip r)
{
    for (int i=0; i<ILOSC_REJESTROW; ++i) r[i] = 0;
}

void podglad_rejestrow(ip r, ui po)
{
    for (int i=0; i<ILOSC_REJESTROW/po; ++i)
    {
        for (int j=0; j<po; j++)
            printf("R[%2d]:(%10d)  ", i*po+j, r[i*po+j]);
        printf("\n");
    }
}

int32_t int_z_4B(ucp m, ui adr)
{
    int32_t v = 0;
    v |= m[adr];   v <<= 8;
    v |= m[adr+1]; v <<= 8;
    v |= m[adr+2]; v <<= 8;
    v |= m[adr+3];
    return v;
}

ui ui_z_4B(ucp m, ui adr)
{
    ui v = 0;
    v |= m[adr];   v <<= 8;
    v |= m[adr+1]; v <<= 8;
    v |= m[adr+2]; v <<= 8;
    v |= m[adr+3];
    return v;
}

void int_do_4B(int32_t i, ucp m, ui adr)
{
    m[adr]   = (i&0xff000000) >> 24;
    m[adr+1] = (i&0x00ff0000) >> 16;
    m[adr+2] = (i&0x0000ff00) >> 8;
    m[adr+3] = (i&0x000000ff);
}

void A(ip r, ucp m, uc r1, uc ra, ui ofs)
{
    r[r1] += int_z_4B(m, r[ra]+ofs);
}

void AR(ip r, uc r1, uc r2)
{
    r[r1] += r[r2];
}

void S(ip r, ucp m, uc r1, uc ra, ui ofs)
{
    r[r1] -= int_z_4B(m, r[ra]+ofs);
}

void SR(ip r, uc r1, uc r2)
{
    r[r1] -= r[r2];
}

void M(ip r, ucp m, uc r1, uc ra, ui ofs)
{
    r[r1] *= int_z_4B(m, r[ra]+ofs);
}

void MR(ip r, uc r1, uc r2)
{
    r[r1] *= r[r2];
}

void D(ip r, ucp m, uc r1, uc ra, ui ofs)
{
    r[r1] /= int_z_4B(m, r[ra]+ofs);
}

void DR(ip r, uc r1, uc r2)
{
    r[r1] /= r[r2];
}

void C(ip r, ucp m, uint32_t* rsp, uc r1, uc ra, ui ofs)
{
    int32_t roznica = r[r1] - int_z_4B(m, r[ra]+ofs);
    *rsp &= 0xfffffffc; // Zerowanie ostatniego polbajtu (z flagami)
    if (roznica > 0) *rsp |= 0b01;
    else if (roznica < 0) *rsp |= 0b10;
}

void CR(ip r, uint32_t* rsp, uc r1, uc r2)
{
    int32_t roznica = r[r1] - r[r2];
    *rsp &= 0xfffffffc; // Zerowanie ostatniego polbajtu (z flagami)
    if (roznica > 0) *rsp |= 0b01;
    else if (roznica == 0) *rsp |= 0b00;
    else *rsp |= 0b10;
}

void L(ip r, ucp m, uc r1, uc ra, ui ofs)
{
    r[r1] = int_z_4B(m, r[ra]+ofs);
}

void LR(ip r, uc r1, uc r2)
{
    r[r1] = r[r2];
}

void ST(ip r, ucp m, uc r1, uc ra, ui ofs)
{
    int_do_4B(r[r1], m, r[ra]+ofs);
}

void LA(ip r, ucp m, uc r1, uc ra, ui ofs)
{
    r[r1] = POLOZENIE_DANYCH+ofs;
}

void ustaw_wskaznik_rozkazu(uip rsp, uint16_t na)
{
    *rsp &= 0x00000003; // Zerowanie poprzedniego wr
    *rsp |= na << 16;
}

ui wartosc_wskaznika_rozkazu(uip rsp)
{
    return (*rsp & 0xffff0000) >> 16;
}

void zwieksz_wskaznik_rozkazu(uip rsp, uint16_t o)
{
    uint16_t poprz = wartosc_wskaznika_rozkazu(rsp);
    *rsp &= 0x00000003; // Zerowanie poprzedniego wr bez zmiany flag
    *rsp |= (poprz + o) << 16;
}

void J(uint32_t* rsp, uint32_t nadr)
{
    *rsp &= 0x0000ffff;
    *rsp |= (nadr << 16);
}

uc JP(uint32_t* rsp, uint32_t nadr)
{
    if (*rsp & 0b01)
    {
        *rsp &= 0x0000ffff;
        *rsp |= (nadr << 16);
        return 1;
    }
    return 0;
}

uc JN(uint32_t* rsp, uint32_t nadr)
{
    if (*rsp & 0b10)
    {
        *rsp &= 0x0000ffff;
        *rsp |= (nadr << 16);
        return 1;
    }
    return 0;
}

uc JZ(uint32_t* rsp, uint32_t nadr)
{
    if ((*rsp & 0x00000003) == 0)
    {
        *rsp &= 0x0000ffff;
        *rsp |= (nadr << 16);
        return 1;
    }
    return 0;
}

void PRINTRS(ip r)
{
    podglad_rejestrow(r, 4);
}

void PRINTR(ip r, uc r1)
{
    printf("R[%2d] = %d\n", r1, r[r1]);
}

void PRINTM(ip r, ucp m, uc r1, uc ra, ui ofs)
{
    printf("M[%4d(%2d):+4] = %d\n", ofs, ra, int_z_4B(m, r[ra]+ofs));
}

void INPUTR(ip r, uc r1, uc ra)
{
    int32_t v;
    printf("R[%2d] <- ", r1); scanf("%d", &v);
    r[r1] = v;
}

void INPUTM(ip r, ucp m, uc r1, uc ra, ui ofs)
{
    int32_t v;
    printf("M[%4d(%2d):+4] <- ", ofs, ra); scanf("%d", &v);
    int_do_4B(v, m, r[ra]+ofs);
}



void wczytaj_program(FILE* plik, ucp m)
{
    // Odnalezienie dlugosci pliku i wczytanie do pomocniczej tablicy
    fseek(plik, 0, SEEK_END); 
    ui dlugosc_pliku = ftell(plik);
    rewind(plik);
    char* zawartosc = (char *) malloc((dlugosc_pliku+1)*sizeof(char));
    fread(zawartosc, dlugosc_pliku, 1, plik);
    fclose(plik);
    // Koniec uzycia pliku (im krocej tym lepiej gdyby cos poszlo nie tak)

    uint32_t dlugosc_danych, dlugosc_rozkazow, pozycja_czytania=0;
    // TODO: sprawdzenie poprawnosci naglowka pliku 
    pozycja_czytania += DLUGOSC_NAGLOWKA_PLIKU;

    dlugosc_danych = int_z_4B(zawartosc, pozycja_czytania); 
    pozycja_czytania += DLUGOSC_INFO_DLDANYCH;
    // TODO: sprawdzenie poprawnosci naglowka danych
    pozycja_czytania += DLUGOSC_NAGLOWKA_DANYCH;

    if (DEBUG) printf("Dlugosc danych: %d, czytanie... ", dlugosc_danych);
    for (int i=0; i<dlugosc_danych; ++i)
    {   
        // Przekopiowanie z zawartosci danych do pamieci
        m[POLOZENIE_DANYCH+i] = zawartosc[pozycja_czytania];
        // Adekwatne zwiekszenie pozycji czytania
        pozycja_czytania++; // (tu czytamy i kopiujemy po bajcie naraz)
    }
    if (DEBUG) printf("Wczytano.\n");

    dlugosc_rozkazow = int_z_4B(zawartosc, pozycja_czytania);
    pozycja_czytania += DLUGOSC_INFO_DLROZKAZ;
    // TODO: sprawdzenie poprawnosci naglowka rozkazow
    pozycja_czytania += DLUGOSC_NAGLOWKA_ROZKAZOW;

    if (DEBUG) printf("Dlugosc rozkazow: %d, czytanie... ", dlugosc_rozkazow);
    for (int i=0; i<dlugosc_rozkazow; ++i)
    {
        // Przekopiowanie z zawartosci rozkazow do pamieci
        m[POLOZENIE_ROZKAZOW+i] = zawartosc[pozycja_czytania];
        // Adekwatne zwiekszenie pozycji czytania
        pozycja_czytania++; // (jednakowo jak wczesniej po bajcie naraz)
    }
    if (DEBUG) printf("Wczytano.\n");
}

int8_t wykonaj_rozkaz(ip r, ucp m, uip rsp)
{
    uint16_t wr = wartosc_wskaznika_rozkazu(rsp);
    uint16_t ofs;

    uc kod = m[wr];

    uc rej1 = (m[wr+1] & 0xf0) >> 4;
    uc rej2 = m[wr+1] & 0xf; 

    if (DEBUG) printf("wr %d kod %.2x (%c) ... ", wr, kod, kod);

    if (kod >= 'A' && kod <= 'Z') // rej-pam wczytaj 4B
    {
        ofs = 256*m[wr+2] + m[wr+3];

        switch (kod)
        {
            case 'A':  A(r, m, rej1, rej2, ofs); break;
            case 'S':  S(r, m, rej1, rej2, ofs); break;
            case 'M':  M(r, m, rej1, rej2, ofs); break;
            case 'D':  D(r, m, rej1, rej2, ofs); break;
            case 'C':  C(r, m, rsp, rej1, rej2, ofs); break;
            case 'L':  L(r, m, rej1, rej2, ofs); break;
            case 'W': LA(r, m, rej1, rej2, ofs); break;
            case 'X': ST(r, m, rej1, rej2, ofs); break;

            case 'J':     J(rsp, r[rej2]+ofs); goto return_ok; break;
            case 'P': if(JP(rsp, r[rej2]+ofs)) goto return_ok; break;
            case 'N': if(JN(rsp, r[rej2]+ofs)) goto return_ok; break;
            case 'Z': if(JZ(rsp, r[rej2]+ofs)) goto return_ok; break;
        }

        zwieksz_wskaznik_rozkazu(rsp, 4);
    }
    else if (kod >= 'a' && kod <= 'z')
    {
        switch (kod)
        {
            case 'a':  AR(r, rej1, rej2); break;
            case 's':  SR(r, rej1, rej2); break;
            case 'm':  MR(r, rej1, rej2); break;
            case 'd':  DR(r, rej1, rej2); break;
            case 'c':  CR(r, rsp, rej1, rej2); break;
            case 'l':  LR(r, rej1, rej2); break;

            return 0;
        }

        zwieksz_wskaznik_rozkazu(rsp, 2);
    }
    else if (kod >= '0' && kod <= '4')
    {
        switch (kod)
        {
            case '0': goto return_stop; break;
            case '1': break;
            case '2': PRINTRS(r); break;
        }

        zwieksz_wskaznik_rozkazu(rsp, 2);
    }
    else if (kod >= '5' && kod <= '9')
    {
        ofs = 256*m[wr+2] + m[wr+3];
        switch (kod)
        {
            case '6': INPUTR(r, rej1, rej2); break;
            case '7': INPUTM(r, m, rej1, rej2, ofs); break;
            case '8': PRINTR(r, rej1); zwieksz_wskaznik_rozkazu(rsp, 2); goto return_ok; 
            case '9': PRINTM(r, m, rej1, rej2, ofs); break;
        }

        zwieksz_wskaznik_rozkazu(rsp, 4);
    }
    else return 0;

    return_ok:
    if (DEBUG) printf(" rozkaz! \n");
    return 1;

    return_stop:
    if (DEBUG) printf(" rozkaz! \n");
    return 0;
}

uint8_t str_zawiera(const char* str, char znak)
{
    for (int i=0; ;i++)
    {
        if (str[i]=='\0') break;
        if (str[i]==znak) return 1;
    }
    return 0;
}

int main(int argc, char const *argv[])
{
    char* nazwa_pliku;
    FILE* plik;

    int32_t rej[ILOSC_REJESTROW];
    uint8_t mem[ROZMIAR_PAMIECI];
    uint32_t rsp = 0;

    ui wykonane_rozkazy;

    if (argc < 2)
    {
        printf("Nie podano programu wykonywalnego.\n");
        printf("Sposob uzycia:\n\temu [nazwa_programu]\n");
        return 3;
    }

    if (argc > 2)
    {
        if (str_zawiera(argv[2], 'd')) DEBUG = 1;
    }
        
    nazwa_pliku = (char*) argv[1];

    zeruj_pamiec(mem);
    zeruj_rejestry(rej);

    plik = fopen(nazwa_pliku, "rb");
    wczytaj_program(plik, mem);

    if (DEBUG) 
    {
        printf("Podglad zaladowanych danych: \n");
        podglad_slow(mem, POLOZENIE_DANYCH, 16, 8);

        printf("Podglad zaladowanych rozkazow: \n");
        podglad_slow(mem, POLOZENIE_ROZKAZOW, 18, 24);
    }

    rej[REJ_POLOZENIA_DANYCH] = POLOZENIE_DANYCH;
    rej[REJ_POLOZENIA_ROZKAZOW] = POLOZENIE_ROZKAZOW;

    ustaw_wskaznik_rozkazu(&rsp, POLOZENIE_ROZKAZOW);
    if (DEBUG) printf("wskaznik rozkazu: %u \n", wartosc_wskaznika_rozkazu(&rsp));

    wykonane_rozkazy = 0; 
    while (wykonaj_rozkaz(rej, mem, &rsp))
    { 
        //podglad_rejestrow(rej, 4);
        if(wykonane_rozkazy > MAX_WYKONANYCH_ROZKAZOW) break;
        wykonane_rozkazy++; 
    }
  
    if (DEBUG) 
    {
        printf("Podglad wynikowych danych: \n");
        podglad_slow(mem, POLOZENIE_DANYCH, 16, 8);
    }

    return 0;
}
